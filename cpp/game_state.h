#pragma once

#include <iostream>
#include <string>
#include <map>
#include <jsoncons/json.hpp>
#include "game_info.h"

struct GameState
{
	struct Car
	{
		std::string name, color;
		double angle;
		bool onTrack;

		struct Pos
		{
			unsigned int pieceIndex;
			double inPieceDistance;
			unsigned int startLaneIndex;
			unsigned int endLaneIndex;
		} pos;

		double speed;
		double acceleration;

		Car();
	};
	std::map<std::string, Car> cars;

	GameState() = default;
	GameState(const GameInfo& info, const jsoncons::json& data, const GameState& old);
};
