#include "game_info.h"

const double PI = 3.14159265359;

/*GameInfo::Track::Piece::OptimalLane GameInfo::Track::Piece::getOptimalLane(
	unsigned int fromLane, unsigned int lapsRemaining) const
{
	auto p = std::make_pair(fromLane, lapsRemaining);

	if(optimalLaneCache.count(p))
		return optimalLaneCache.at(p);

	if(lapsRemaining == 0)
		return OptimalLane(0.0, 0);

	unsigned int nextIndex = index + 1;
	unsigned int lapsRemainingNext = lapsRemaining;

	if(nextIndex == track.pieces.size())
	{
		nextIndex = 0;
		lapsRemainingNext--;
	}

	if(!laneSwap)
	{
		OptimalLane optimalLane = track.pieces[nextIndex].getOptimalLane(fromLane, lapsRemainingNext);
		optimalLane.first += laneLengths[fromLane];

		return optimalLaneCache[p] = optimalLane;
	}

	std::vector<OptimalLane> options;

	unsigned int left = fromLane;
	unsigned int right = fromLane;

	if(left != 0) left--;
	if(right != track.lanes.size()) right++;

	for(unsigned int i = left; i <= right; i++)
	{
		options.push_back(OptimalLane(
			track.pieces[nextIndex].getOptimalLane(i, lapsRemainingNext).first + length,
			i
		));
	}

	return optimalLaneCache[p] = *std::min_element(options.begin(), options.end());
}*/

void GameInfo::parse(const jsoncons::json& race)
{
	const auto& trackJson = race["track"];
	const auto& lanesJson = trackJson["lanes"];
	const auto& piecesJson = trackJson["pieces"];
	const auto& carsJson = race["cars"];
	const auto& raceSessionJson = race["raceSession"];

	track.id = trackJson["id"].as<std::string>();
	track.name = trackJson["name"].as<std::string>();

	track.lanes.resize(lanesJson.size());
	for(unsigned int i = 0; i < track.lanes.size(); i++)
	{
		auto& lane = track.lanes[i];

		lane.distanceFromCenter = lanesJson[i]["distanceFromCenter"].as<int>();
		lane.index = lanesJson[i]["index"].as<unsigned int>();
	}

	//track.pieces.resize(piecesJson.size(), Track::Piece(track));
	track.pieces.resize(piecesJson.size());
	track.cornerCount = 0;
	for(unsigned int i = 0; i < track.pieces.size(); i++)
	{
		auto& piece = track.pieces[i];

		piece.index = i;

		if(piecesJson[i].has_member("angle"))
		{
			track.cornerCount++;

			piece.type = Track::Piece::Type::BEND;
			piece.radius = piecesJson[i]["radius"].as<double>();
			piece.angle = piecesJson[i]["angle"].as<double>();

			piece.length = abs(piece.angle) / 360 * PI * 2 * piece.radius;

			piece.laneLengths.resize(track.lanes.size());
			piece.laneRadiuses.resize(track.lanes.size());
			for(unsigned int j = 0; j < track.lanes.size(); j++)
			{
				piece.laneRadiuses[j] = piece.radius;

				if(piece.angle >= 0) // Bend right
				{
					piece.laneRadiuses[j] -= track.lanes[j].distanceFromCenter;
				}
				else // Bend left
				{
					piece.laneRadiuses[j] += track.lanes[j].distanceFromCenter;
				}

				piece.laneLengths[j] = abs(piece.angle) / 360 * PI * 2 * piece.laneRadiuses[j];
			}
		}
		else
		{
			piece.type = Track::Piece::Type::STRAIGHT;
			piece.length = piecesJson[i]["length"].as<double>();

			piece.laneLengths.resize(track.lanes.size());
			for(unsigned int j = 0; j < track.lanes.size(); j++)
				piece.laneLengths[j] = piece.length;
		}
		
		piece.laneSwap = piecesJson[i].get("switch", false).as<bool>();
	}

	cars.resize(carsJson.size());
	for(unsigned int i = 0; i < cars.size(); i++)
	{
		cars[i].name = carsJson[i]["id"]["name"].as<std::string>();
		cars[i].color = carsJson[i]["id"]["color"].as<std::string>();
		cars[i].length = carsJson[i]["dimensions"]["length"].as<double>();
		cars[i].width = carsJson[i]["dimensions"]["width"].as<double>();
		cars[i].guideFlagPosition = carsJson[i]["dimensions"]["guideFlagPosition"].as<double>();
	}

	raceSession.laps = raceSessionJson["laps"].as<unsigned int>();
	raceSession.maxLapTime = raceSessionJson["maxLapTimeMs"].as<unsigned int>();
	raceSession.quickRace = raceSessionJson["quickRace"].as<bool>();
}

void GameInfo::print()
{
	std::cout << "Track: " << track.name << " (" << track.id << ")" << std::endl << std::endl;
	std::cout << "Laps: " << raceSession.laps
		<< " - Max lap time in ms: " << raceSession.maxLapTime
		<< " - Quick race: " << raceSession.quickRace << std::endl;

	std::cout << "Lanes (" << track.lanes.size() << "):" << std::endl;
	for(auto const& lane : track.lanes)
	{
		std::cout << "Index: " << lane.index
			<< " - Distance from center: " << lane.distanceFromCenter << std::endl;
	}
	std::cout << std::endl;

	std::cout << "Pieces (" << track.pieces.size() << "):" << std::endl;
	for(auto const& piece : track.pieces)
	{
		std::cout << piece.length;

		if(piece.type == Track::Piece::Type::BEND)
			std::cout << " turn " << piece.radius << " (" << piece.angle << " degrees)";

		if(piece.laneSwap)
			std::cout << " (Switch)";
		std::cout << std::endl;
	}
	std::cout << std::endl;

	std::cout << "Cars (" << cars.size() << "):" << std::endl;
	for(Car const& car : cars)
	{
		std::cout << car.name << " (" << car.color << ") - "
			<< "Length: " << car.length
			<< " - Width: " << car.width
			<< " - Guide flag position: " << car.guideFlagPosition << std::endl;
	}
	std::cout << std::endl;
}