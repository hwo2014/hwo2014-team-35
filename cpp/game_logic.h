#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>
#include "game_info.h"
#include "game_state.h"

class GameLogic
{
public:
	typedef std::vector<jsoncons::json> msg_vector;

	GameLogic();
	msg_vector react(const jsoncons::json& msg);

private:
	typedef void(GameLogic::* action_fun)(const jsoncons::json&);
	static const std::map<std::string, action_fun> action_map;

	unsigned int nextPiece(unsigned int p) const;
	std::vector<unsigned int> getNextCorners(unsigned int curPiece, unsigned int count = 3) const;
	double getDistance(unsigned int lane, unsigned int pieceA, unsigned int pieceB,
		double inPieceDistA = 0.0, double inPieceDistB = 0.0) const;

	void onJoin(const jsoncons::json& data);
	void onYourCar(const jsoncons::json& data);
	void onGameStart(const jsoncons::json& data);
	void onGameInit(const jsoncons::json& data);
	void onCarPositions(const jsoncons::json& data);
	void onCrash(const jsoncons::json& data);
	void onGameEnd(const jsoncons::json& data);
	void onError(const jsoncons::json& data);

	std::string myColor;

	double targetSpeed; // units / tick
	unsigned int targetLane;

	GameInfo gameInfo;
	GameState gameState;

	unsigned int gameTick;
	unsigned int lastSentLane;
	double throttle; // Filled in when trying to reach a certain speed
	double slowDownMult;

};

#endif
