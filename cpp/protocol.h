#ifndef HWO_PROTOCOL_H
#define HWO_PROTOCOL_H

#include <string>
#include <iostream>
#include <jsoncons/json.hpp>

namespace hwo_protocol
{
  jsoncons::json makeRequest(const std::string& msgType, const jsoncons::json& data);
  jsoncons::json makeJoin(const std::string& name, const std::string& key);
  jsoncons::json makePing();
  jsoncons::json makeThrottle(double throttle);
  jsoncons::json makeLaneSwitch(std::string const& dir);
}

#endif
