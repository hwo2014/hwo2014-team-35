#include "game_logic.h"
#include "protocol.h"

using namespace hwo_protocol;

const double bigSpeed = 1000.0;
//const double magicConstant = 0.675;
const double magicConstant = 0.68;

const std::map<std::string, GameLogic::action_fun> GameLogic::action_map = {
	{ "join", &GameLogic::onJoin },
	{ "yourCar", &GameLogic::onYourCar },
	{ "gameStart", &GameLogic::onGameStart },
	{ "gameInit", &GameLogic::onGameInit },
	{ "carPositions", &GameLogic::onCarPositions },
	{ "crash", &GameLogic::onCrash },
	{ "gameEnd", &GameLogic::onGameEnd },
	{ "error", &GameLogic::onError }
};

GameLogic::GameLogic()
{
	targetSpeed = bigSpeed;
	targetLane = -1;

	lastSentLane = -1;
	throttle = 0.0;
	slowDownMult = 0.999;
}

GameLogic::msg_vector GameLogic::react(const jsoncons::json& msg)
{
	const auto& msgType = msg["msgType"].as<std::string>();
	const auto& data = msg["data"];

	//std::cout << msg << std::endl;

	if(msg.has_member("gameTick"))
	{
		gameTick = msg["gameTick"].as<unsigned int>();
	}

	auto action_it = action_map.find(msgType);
	if(action_it != action_map.end())
	{
		(this->*(action_it->second))(data);
	}
	else
	{
		std::cout << "Unknown message type: " << msgType << std::endl;
		return {};
	}

	if(targetLane != -1 && lastSentLane != targetLane)
	{
		lastSentLane = targetLane;

		if(targetLane < gameState.cars[myColor].pos.startLaneIndex)
		{
			return{ makeLaneSwitch("Left") };
		}
		else
		{
			return{ makeLaneSwitch("Right") };
		}
	}

	if(msgType == "carPositions")
	{
		double diff = targetSpeed - gameState.cars[myColor].speed;
		double speedThrottle = targetSpeed / 10.0;

		if(diff > 0.0) throttle = std::min(std::max(speedThrottle, diff * 25), 1.0);
		else throttle = 0.0;

		return { makeThrottle(throttle) };
	}
	else if(msgType == "crash")
	{
		return { makePing() };
	}
	
	return {};
}

unsigned int GameLogic::nextPiece(unsigned int p) const
{
	return (p + 1) % gameInfo.track.pieces.size();
}

std::vector<unsigned int> GameLogic::getNextCorners(unsigned int curPiece, unsigned int count) const
{
	curPiece %= gameInfo.track.pieces.size();

	std::vector<unsigned int> res;

	unsigned int i = curPiece;

	for(unsigned int j = 0; j < count; j++)
	{
		while(gameInfo.track.pieces[i].type != GameInfo::Track::Piece::BEND)
		{
			i++;
			i %= gameInfo.track.pieces.size();

			if(i == curPiece) return res;
		}
		
		res.push_back(i);

		i++;
		i %= gameInfo.track.pieces.size();

		if(i == curPiece) return res;
	}

	return res;
}

double GameLogic::getDistance(unsigned int lane, unsigned int pieceA, unsigned int pieceB,
	double inPieceDistA, double inPieceDistB) const
{
	if(pieceA == pieceB) return 0.0;

	unsigned int piece = nextPiece(pieceA);

	double distance = 0.0;
	distance += gameInfo.track.pieces[pieceA].laneLengths[lane] - inPieceDistA;

	while(piece != pieceB)
	{
		distance += gameInfo.track.pieces[piece].laneLengths[lane];
		piece = nextPiece(piece);
	}

	distance += inPieceDistB;
	
	return distance;
}

void GameLogic::onJoin(const jsoncons::json& data)
{
	std::cout << "Joined" << std::endl;
}

void GameLogic::onYourCar(const jsoncons::json& data)
{
	std::cout << "Go my car" << std::endl;

	myColor = data["color"].as<std::string>();
}

void GameLogic::onGameStart(const jsoncons::json& data)
{
	std::cout << "Race started" << std::endl;
}

void GameLogic::onGameInit(const jsoncons::json& data)
{
	gameInfo.parse(data["race"]);
	gameInfo.print();
}

void GameLogic::onCarPositions(const jsoncons::json& data)
{
	double oldSpeed = gameState.cars[myColor].speed;
	gameState = GameState(gameInfo, data, gameState);
	const GameState::Car& myCar = gameState.cars[myColor];

	std::cout << gameState.cars[myColor].speed << " " << gameState.cars[myColor].acceleration << "\n";

	if(throttle == 0.0 && oldSpeed != 0.0)
	{
		slowDownMult = myCar.speed / oldSpeed;
	}

	targetLane = 1;

	
	unsigned int myLane = myCar.pos.startLaneIndex;
	unsigned int myPiece = myCar.pos.pieceIndex;

	auto corners = getNextCorners(myCar.pos.pieceIndex);

	double speedLimit = bigSpeed;

	for(unsigned int cornerId : corners)
	{
		auto const& corner = gameInfo.track.pieces[cornerId];
		double dist = getDistance(myLane, myPiece, cornerId, myCar.pos.inPieceDistance);

		double cornerSpeed = sqrt(corner.laneRadiuses[myLane]) * magicConstant;
		double timeNeeded = log(cornerSpeed / myCar.speed) / log(slowDownMult);
		double distanceNeeded = ((pow(slowDownMult, timeNeeded) - 1) * myCar.speed) / log(slowDownMult);

		if(dist == 0.0 || dist <= distanceNeeded)
		{
			/*std::cout << "Corner " << corner.angle << " degrees, radius " << corner.laneRadiuses[myLane]
				<< " - Distance: " << dist << " Distance needed: " << distanceNeeded << "\n";*/

			speedLimit = std::min(cornerSpeed, speedLimit);
		}
	}

	targetSpeed = speedLimit;
	std::cout << "Target: " << speedLimit << std::endl;
}

void GameLogic::onCrash(const jsoncons::json& data)
{
	std::cout << "Someone crashed" << std::endl;
}

void GameLogic::onGameEnd(const jsoncons::json& data)
{
	std::cout << "Race ended" << std::endl;
}

void GameLogic::onError(const jsoncons::json& data)
{
	std::cout << "Error: " << data.to_string() << std::endl;
}
