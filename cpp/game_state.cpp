#include "game_state.h"

GameState::Car::Car()
{
	angle = 0.0;
	onTrack = false;
	pos.pieceIndex = 0;
	pos.inPieceDistance = 0.0;
	pos.startLaneIndex = 0;
	pos.endLaneIndex = 0;
	speed = 0.0;
	acceleration = 0.0;
}

GameState::GameState(const GameInfo& info, const jsoncons::json& data, const GameState& old)
{
	for(unsigned int i = 0; i < data.size(); i++)
	{
		const auto& carData = data[i];
		const auto& posData = carData["piecePosition"];
		std::string color = carData["id"]["color"].as<std::string>();

		Car& car = cars[color];
		car.name = carData["id"]["name"].as<std::string>();
		car.color = color;
		car.angle = carData["angle"].as<double>();
		car.onTrack = true;

		car.pos.pieceIndex = posData["pieceIndex"].as<unsigned int>();
		car.pos.inPieceDistance = posData["inPieceDistance"].as<double>();
		car.pos.startLaneIndex = posData["lane"]["startLaneIndex"].as<unsigned int>();
		car.pos.endLaneIndex = posData["lane"]["endLaneIndex"].as<unsigned int>();
	}

	// Sadly this code breaks if car goes over a round in a tick

	for(auto& p : cars)
	{
		Car& car = p.second;

		// Car has crashed
		if(old.cars.count(car.color) == 0) continue;

		const Car& oldCar = old.cars.at(car.color);

		if(!oldCar.onTrack) continue;

		if(car.pos.pieceIndex == oldCar.pos.pieceIndex
			&& car.pos.inPieceDistance >= oldCar.pos.inPieceDistance)
		{
			car.speed = car.pos.inPieceDistance - oldCar.pos.inPieceDistance;
		}
		else
		{
			unsigned int pieceCount = info.track.pieces.size();

			car.speed = info.track.pieces[oldCar.pos.pieceIndex].laneLengths[oldCar.pos.startLaneIndex]
				- oldCar.pos.inPieceDistance;

			unsigned int nextPieceFromOld = (oldCar.pos.pieceIndex + 1) % pieceCount;
			if(nextPieceFromOld != car.pos.pieceIndex) // Car is going super fast
			{
				for(unsigned int i = nextPieceFromOld; i != car.pos.pieceIndex; i++, i %= pieceCount)
				{
					car.speed += info.track.pieces[i].length;
				}
			}

			car.speed += car.pos.inPieceDistance;

			if(car.speed < 0)
			{
				std::cout << "ERROR" << std::endl;
			}
		}

		car.acceleration = car.speed - oldCar.speed;
	}
}