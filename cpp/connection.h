#ifndef HWO_CONNECTION_H
#define HWO_CONNECTION_H

#include <string>
#include <iostream>
#include <boost/asio.hpp>
#include <jsoncons/json.hpp>

using boost::asio::ip::tcp;

class HWOConnection
{
public:
  HWOConnection(const std::string& host, const std::string& port);
  ~HWOConnection();
  jsoncons::json receiveResponse(boost::system::error_code& error);
  void sendRequests(const std::vector<jsoncons::json>& msgs);

private:
  boost::asio::io_service io_service;
  tcp::socket socket;
  boost::asio::streambuf response_buf;
};

#endif