#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <jsoncons/json.hpp>

struct GameInfo
{
	struct Track
	{
		std::string id;
		std::string name;

		struct Lane
		{
			int distanceFromCenter;
			unsigned int index;
		};
		std::vector<Lane> lanes;

		struct Piece
		{
			//Track const& track;

			enum Type { STRAIGHT, BEND } type;

			unsigned int index;
			double length;
			std::vector<double> laneLengths;
			double radius, angle; // For bend pieces
			std::vector<double> laneRadiuses;

			bool laneSwap;

			/*// Pair is: optimal lane, length until the end of race
			typedef std::pair<double, unsigned int> OptimalLane;

			mutable std::map<std::pair<unsigned int, unsigned int>, OptimalLane> optimalLaneCache;
			OptimalLane getOptimalLane(unsigned int fromLane, unsigned int lapsRemaining) const;
		
			Piece(Track const& _track) : track(_track) {}*/
		};
		std::vector<Piece> pieces;
		unsigned int cornerCount;
	} track;


	struct Car
	{
		std::string name, color;
		double length, width, guideFlagPosition;
	};
	std::vector<Car> cars;


	struct RaceSession
	{
		unsigned int laps;
		unsigned int maxLapTime; // In milliseconds
		bool quickRace;
	} raceSession;


	void parse(const jsoncons::json& race); // Parses GameInfo from JSON race data
	void print();
};
