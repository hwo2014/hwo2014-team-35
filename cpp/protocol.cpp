#include "protocol.h"

namespace hwo_protocol
{

  jsoncons::json makeRequest(const std::string& msg_type, const jsoncons::json& data)
  {
    jsoncons::json r;
    r["msgType"] = msg_type;
    r["data"] = data;
    return r;
  }

  jsoncons::json makeJoin(const std::string& name, const std::string& key)
  {
    jsoncons::json data;
    data["name"] = name;
    data["key"] = key;
    return makeRequest("join", data);
  }

  jsoncons::json makePing()
  {
    return makeRequest("ping", jsoncons::null_type());
  }

  jsoncons::json makeThrottle(double throttle)
  {
    return makeRequest("throttle", throttle);
  }

  jsoncons::json makeLaneSwitch(std::string const& dir)
  {
	  return makeRequest("switchLane", dir);
  }

}  // namespace hwo_protocol
